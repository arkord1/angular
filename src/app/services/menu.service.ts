import { Injectable } from '@angular/core';
import { Menu } from '../interfaces/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {


   menus: Menu[] =[ 
     {
       titulo: "Páginas",
       subMenus: [
         {
           titulo: "About",
           url: "/about"
         },
         {
           titulo: "Home",
           url: "/home"
         }
       ]
    }
  ]

  obtenerMenus(): any[] {
    return this.menus;
  }

  constructor() { }
}
