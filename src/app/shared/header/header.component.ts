import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  menu: any[];

  constructor(Menu: MenuService) { 
    this.menu = Menu.obtenerMenus();
  }

  ngOnInit(): void {
  }

}
