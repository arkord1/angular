import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { PromesasComponent } from './promesas/promesas.component';
import { ObservableComponent } from './observable/observable.component';



@NgModule({
  declarations: [
    HomeComponent,
    AboutComponent,
    ContactComponent,
    DashboardComponent,
    NotfoundComponent,
    PromesasComponent,
    ObservableComponent
  ],
  exports: [
    HomeComponent,
    AboutComponent,
    ContactComponent,
    DashboardComponent,
    NotfoundComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PagesModule { }
