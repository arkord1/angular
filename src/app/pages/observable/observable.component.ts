import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styles: [
  ]
})
export class ObservableComponent implements OnInit {

  constructor() {
    const obs$ = new Observable(observer => {

      let i = -1;

      const intervalo = setInterval(() => {
        console.log("tick");
        i++;
        observer.next(i);

        if(i ==4) {
          clearInterval(intervalo);
          observer.complete();
        }


      }, 1000);

    });

    obs$.subscribe(
      valor => console.log("Subs: ", valor ),
      error => console.warn("error"),
      () => console.info("Terminó la subscripción")
    )

   }

  ngOnInit(): void {
  }

}
